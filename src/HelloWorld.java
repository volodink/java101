//~ import java.util.*;
//~ public class HelloWorld {
	//~ public static void main(String[] args) {
		//~ Scanner in = new Scanner(System.in);
		//~ System.out.print("Enter x: ");
        //~ double x = 0.0;
		//~ try {
			//~ x = in.nextDouble();
			//~ //in.nextLine();
		//~ } catch (InputMismatchException e) {
			//~ System.out.println("Wrong X number format.");
		//~ }
		//~ System.out.print("Enter a: ");
		//~ double a = 0.0;
		//~ try {
			//~ a = in.nextDouble();
		//~ } catch (InputMismatchException e) {
			//~ System.out.println("Wrong A number format.");		
		//~ }
		//~ double result = 0.5 / x * a * Math.sin(0.4);
		//~ System.out.println("Result: " + result);
	//~ }
//~ }

import java.io.*;
import java.lang.*;
public class HelloWorld {
	public static void main(String[] args) {
		Console console = System.console();
		System.out.print("Enter x: ");
        double x = 0.0;
		try {
			String s = console.readLine();
			x = Double.parseDouble(s);
		} catch (IOError | NumberFormatException e) {
			System.out.println("Wrong X number format or IO operation.");
		}
		System.out.print("Enter a: ");
		double a = 0.0;
		try {
			a = Double.parseDouble(console.readLine());
		} catch (IOError | NumberFormatException e) {
			System.out.println("Wrong A number format or IO operation.");
		}
		double result = 0.5 / x * a * Math.sin(0.4);
		System.out.println("Result: " + result);
	}
}
