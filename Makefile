all:	clean init
	clear
	javac -sourcepath src -d build/classes src/HelloWorld.java
	echo "Main-Class: HelloWorld" > Manifest
	jar cfm bin/HelloWorld.jar Manifest -C build/classes .
	tree
run:
	java -jar bin/HelloWorld.jar
init:
	mkdir -p build/classes
	mkdir bin
clean:
	rm -rf build
	rm -rf bin
	rm -f Manifest

